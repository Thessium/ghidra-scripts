#MakeName like in IDA, but a little better.
#Supports creation of namespaces
#Limitations: Can not handle multi level namespaces
#@author florian0
#@category SilkroadOnline
#@keybinding 
#@menupath 
#@toolbar 

from ghidra.program.model.symbol import SourceType

def MakeName(addr, name):
  str_ns, str_fn = name.split("::");
  address = intToAddress(addr)
  label = getSymbolAt(address)
  label.setName(str_fn, SourceType.ANALYSIS)
  label.setNamespace(getNamespace(None, str_ns))

# Example usage
MakeName(0x00401000, "IRegionManager::IRegionManager")
MakeName(0x00401020, "IRegionManager::destruct")
MakeName(0x00401040, "IRegionManager::~IRegionManager")
